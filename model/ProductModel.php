<?php
namespace app\model;
use app\Core\Aplication;
use app\Core\DbModel;
use app\Core\Model;
use app\Core\Request;
use PDO;

class ProductModel extends DbModel
{

    public function insertData(){
            $this->save();
            Aplication::$app->response->redirect('/');
    }

    public function SelectProduct()
    {
        $this->SelectProduct();
    }

    public function RenderProduct(){
        $statement=Aplication::$app->db->pdo->prepare("SELECT *FROM products ORDER BY id DESC ");
        $statement->execute();
        $product = $statement->fetchAll(PDO::FETCH_ASSOC);
        $parameter=['name'=>'luka', 'lastname'=>'datunashvili', 'email'=>'luka@gmail.com'];
        foreach ($product as $prod){
            echo '
                <div class="max-w-sm rounded overflow-hidden shadow-lg border mt-4 delete-checkbox">
                <input type="checkbox" class="w-4 h-4 ml-6 mt-2" name="product_delete_id[]" value="'.$prod["id"].'">
                <div class="px-6 py-4">
                    <div class="text-gray-500 text-sm mb-2">Name:'.$prod["name"].'</div>
                    <div class="text-gray-500 text-sm mb-2">Sku:'.$prod["sku"].'</div>
                    <div class="text-gray-500 text-sm mb-2">Price:$'.$prod["price"].'</div>
                    <div class="text-gray-500 text-sm mb-2">Size:Mb'.$prod["size"].'</div>
                    <div class="text-gray-500 text-sm mb-2">Book:'.$prod["book"].'</div>
                    <div class="text-gray-500 text-sm mb-2">Height:'.$prod["height"].'</div>
                    <div class="text-gray-500 text-sm mb-2">Width:'.$prod["width"].'</div>
                    <div class="text-gray-500 text-sm mb-2">Lenght:'.$prod["length"].'</div>
                </div>
            </div>
            '.PHP_EOL;
        }
    }
    public function DeleteProduct(){
        $all_id=$_POST['product_delete_id'];
        $extracted_id=implode(",",$all_id);
        $statement=Aplication::$app->db->pdo->prepare("DELETE FROM products WHERE id IN($extracted_id)");
        $statement->execute();
        if ($statement){
            Aplication::$app->response->redirect('/');
        }

    }

}