document.getElementById('productType').addEventListener('change', function () {
    var style = this.value == 0 ? 'block' : 'none';
    document.getElementById('hidden_div_dvd').style.display = style;
});
document.getElementById('productType').addEventListener('change', function () {
    var style = this.value == 1 ? 'block' : 'none';
    document.getElementById('hidden_div_book').style.display = style;
});
document.getElementById('productType').addEventListener('change', function () {
    var style = this.value == 2 ? 'block' : 'none';
    document.getElementById('hidden_div_furniture').style.display = style;
});

function Validation(error=[]) {
    const sku=document.getElementById("sku").value.trim();
    const name=document.getElementById("name").value;
    const price=document.getElementById("price").value;
    const size=document.getElementById("size").value;
    const weight=document.getElementById("weight").value;
    const height=document.getElementById("height").value;
    const width=document.getElementById("width").value;
    const length=document.getElementById("length").value;
    console.log(typeof price);
    if (sku === '' && name==='' && price==='' && size==='' && weight==='' && height==='' && width==='' && length==='' ){
        error.push("All Field Is Required !");
        document.getElementById("all-field").classList.remove("hidden");
        return false;
    }
    if (sku === ''){
        error.push("sku-error");
        document.getElementById("data-sku-error").classList.remove("hidden");
        document.getElementById("sku-error").classList.remove("hidden");
        document.getElementById("sku").style.borderColor="red";

    }
    if (name === '' || /[^a-zA-Z0-9\-\/]/g.test(name)){
        error.push("name-error");
        document.getElementById("data-name-error").classList.remove("hidden");
        document.getElementById("name-error").classList.remove("hidden");
        document.getElementById("name").style.borderColor="red";
    }

    if (price === '' || /[^a-zA-Z0-9\-\/]/g.test(price)){
        error.push("price-error");
        document.getElementById("data-price-error").classList.remove("hidden");
        document.getElementById("price-error").classList.remove("hidden");
        document.getElementById("price").style.borderColor="red";
    }
    if (size === '' || /[^a-zA-Z0-9\-\/]/g.test(size)){
        error.push("size-error");
        document.getElementById("data-size-error").classList.remove("hidden");
        document.getElementById("size-error").classList.remove("hidden");
        document.getElementById("size").style.borderColor="red";
    }
    if (weight === '' || /[^a-zA-Z0-9\-\/]/g.test(weight)){
        error.push("weight-error");
        document.getElementById("data-weight-error").classList.remove("hidden");
        document.getElementById("weight-error").classList.remove("hidden");
        document.getElementById("weight").style.borderColor="red";
    }
    if (height === '' || /[^a-zA-Z0-9\-\/]/g.test(height)){
        error.push("height-error");
        document.getElementById("data-height-error").classList.remove("hidden");
        document.getElementById("height-error").classList.remove("hidden");
        document.getElementById("height").style.borderColor="red";
    }
    if (width === '' || /[^a-zA-Z0-9\-\/]/g.test(width)){
        error.push("width-error");
        document.getElementById("data-weight-error").classList.remove("hidden");
        document.getElementById("width-error").classList.remove("hidden");
        document.getElementById("width").style.borderColor="red";
    }
    if (length === '' || /[^a-zA-Z0-9\-\/]/g.test(length)){
        error.push("length-error");
        document.getElementById("data-length-error").classList.remove("hidden");
        document.getElementById("length-error").classList.remove("hidden");
        document.getElementById("length").style.borderColor="red";
    }


    }
const form=document.getElementById("product_form");
form.addEventListener('submit', e => {
    Validation(error=[]);
    if (error.length>0){
        e.preventDefault();
    }
});
