<?php
/**
 * Hades11
 */
use app\controller\ProductController;
use app\Core\Aplication;

require_once __DIR__ . '/../vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

$config=['db'=>['dns'=>$_ENV['DB_DNS'], 'user'=>$_ENV['DB_USER'], 'password'=>$_ENV['DB_PASSWORD'],]];
$app=new Aplication(dirname(__DIR__),$config);

$app->router->get('/',[new ProductController(),'ProductList']);
$app->router->get('/add-product',[new  ProductController(),'AddProduct']);
$app->router->post('/save',[new ProductController(),'handleProduct']);
$app->router->post('/delete',[new ProductController(),'MassDelete']);


$app->run();