<?php

namespace app\Core;

class Database
{
    public \PDO $pdo;

    public function __construct(array $config)
    {
        $dns=$config['dns'] ?? '';
        $user=$config['user'] ?? '';
        $password=$config['password'] ?? '';
        $this->pdo=new \PDO($dns,$user,$password);
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
    }

    public function applyMigration()
    {
        $this->CreateMigrationTable();
        $appliedMigration=$this->GetapplyMigration();
        $newMigration=[];
        $file=scandir(Aplication::$ROOT_DIR.'/migrations');
       $toAppliedMigration= array_diff($file,$appliedMigration);

       foreach ($toAppliedMigration as $migration){
           if ($migration === '.' || $migration === '..'){
               continue;
           }
           require_once Aplication::$ROOT_DIR.'/migrations/'.$migration;
           $className=pathinfo($migration,PATHINFO_FILENAME);
           $instance=new $className();
           $this->log("Applied Migration $migration");
           $instance->up();
           $this->log("Applied Migration $migration");
           $newMigration[]=$migration;
       }
       if (!empty($newMigration)){
           $this->saveMigration($newMigration);
       }else{
           $this->log("All Migration Are Applied !");
       }
    }

    public function CreateMigrationTable(){
        $this->pdo->exec("CREATE TABLE IF NOT EXISTS migrations(
        id INT AUTO_INCREMENT PRIMARY KEY,migration VARCHAR (255),created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ) ENGINE=INNODB;");
    }

    public function GetapplyMigration(){
       $statement=$this->pdo->prepare("SELECT migration FROM migrations");
       $statement->execute();
       return $statement->fetchAll(\PDO::FETCH_COLUMN);
    }

    public  function saveMigration(array $migration){
        $str=implode(",",array_map(fn($m)=>"('$m')",$migration));

        $statement=$this->pdo->prepare("INSERT INTO migrations (migration) VALUES 
            $str
            ");

         $statement->execute();
    }

    protected function log($message){
        echo '['.date('Y-m-d H:i:s').']-'.$message.PHP_EOL;
    }
}