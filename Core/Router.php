<?php
namespace app\Core;

class Router
{
    protected  array $routes=[];
    public Request $request;
    public Response $response;

    public function __construct(Request $request,Response $response )
    {
        $this->request = $request;
        $this->response=$response;
    }

    public function get($path,$callback){

        $this->routes['get'][$path]=$callback;

    }

    public function post($path,$callback){

        $this->routes['post'][$path]=$callback;

    }

    public function resolve()
    {
       $path= $this->request->getPath();
       $method=$this->request->method();
       $callback=$this->routes[$method][$path] ?? false;
       if ($callback === false){
           $this->response->SetStatusCode(404);
           return $this->renderView("_404");
           exit();
       }
       if (is_string($callback)){
           return $this->renderView($callback);
       }
       echo call_user_func($callback,$this->request);
    }

    public function renderView($view,$params=[])
    {
        $layoutContent=$this->layoutContent();
        $viewContent=$this->renderOnlyView($view,$params);
        echo str_replace('{{content}}',$viewContent,$layoutContent);
    }

    protected function layoutContent()
    {
        ob_start();
        include_once Aplication::$ROOT_DIR."/views/layouts/main.php";
        return ob_get_clean();
    }

    protected function renderOnlyView($view,$params)
    {
//        var_dump($params);
        foreach ($params as $key=>$value){
            $$key=$value;
        }
        ob_start();
        include_once Aplication::$ROOT_DIR."/views/$view.php";
        return ob_get_clean();
    }

    public function renderContent($viewContent)
    {
        $layoutContent=$this->layoutContent();
        echo str_replace('{{content}}',$viewContent,$layoutContent);
    }


}