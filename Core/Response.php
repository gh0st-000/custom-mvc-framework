<?php

namespace app\Core;

class Response
{
    public function SetStatusCode(int $code)
    {
        http_response_code($code);
    }

    public function redirect(string $url)
    {
        header('Location: '.$url);
    }

}