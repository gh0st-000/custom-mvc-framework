<?php

use app\Core\Aplication;

require_once __DIR__ . '/vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$config = ['db' => ['dns' => $_ENV['DB_DNS'], 'user' => $_ENV['DB_USER'], 'password' => $_ENV['DB_PASSWORD'],]];
$app = new Aplication(__DIR__, $config);

$app->db->applyMigration();