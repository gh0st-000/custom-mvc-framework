<?php

use app\Core\Aplication;

class  m0001_initial{
    public function up()
    {
       $db=Aplication::$app->db;
       $SQL="CREATE TABLE products(
        id INT  AUTO_INCREMENT PRIMARY KEY ,
        sku VARCHAR(255) NOT NULL ,
        name VARCHAR(255) NOT NULL ,
        price VARCHAR(255) NOT NULL ,
        size VARCHAR(255) NOT NULL ,
        book VARCHAR(255) NOT NULL ,
        height VARCHAR(255) NOT NULL ,
        width VARCHAR(255) NOT NULL ,
        length VARCHAR(255) NOT NULL ,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
        ) ENGINE=INNODB;";
        $db->pdo->exec($SQL);
    }

    public function down()
    {
        $db=Aplication::$app->db;
        $SQL="DROP TABLE products;";
        $db->pdo->exec($SQL);
    }
}
