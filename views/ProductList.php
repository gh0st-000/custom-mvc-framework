
<header>
    <form method="post" action="/delete">
    <nav class="">
        <div class="max-w-screen-2xl mx-auto px-2 sm:px-6 lg:px-8 border-b border-gray-900">
            <div class="relative flex items-center justify-between h-16">
                <div class="absolute inset-y-0 left-0 flex items-center sm:hidden">
                    <button type="button" onclick="OpenMobileNav()" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white" aria-controls="mobile-menu" aria-expanded="false">
                        <span class="sr-only">Open main menu</span>
                        <svg class="block h-6 w-6 text-gray-900" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                        </svg>
                    </button>
                </div>
                <div class="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                    <div class="flex-shrink-0 flex items-center">

                    </div>
                    <div class="hidden sm:block">
                        <div class="flex space-x-4">
                            <span  class="text-xl px-3 py-2 font-medium text-sm font-medium" aria-current="page">Product List</span>
                        </div>
                    </div>
                </div>
                <div class="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                    <div class="ml-3 relative">
                        <div class="flex justify-between space-x-3">
                            <a href="/add-product" type="button"
                               class="px-3 py-2 bg-gray-800 text-white flex text-sm rounded-md focus:outline-none  border border-white hover:bg-green-600"
                               id="user-menu-button" aria-expanded="false" aria-haspopup="true">
                                Add
                            </a>
                            <button type="submit"
                                    class="px-3 py-2 bg-gray-800 text-white flex text-sm rounded-md focus:outline-none  border border-white hover:bg-red-600"
                                    id="delete-product-btn" aria-expanded="false" aria-haspopup="true">
                                Mass Delete
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sm:hidden" id="mobile-menu">
            <div class="px-2 pt-2 pb-3 space-y-1 hidden" id="mobile-nav">
                <a href="#" class="bg-gray-900 text-white block px-3 py-2 rounded-md text-base font-medium" aria-current="page">Product List</a>
            </div>
        </div>
        <div class="max-w-screen-2xl mx-auto py-10 px-10 border">
                <div class="grid sm:grid-cols-4 sm:gap-4 w-full space-y-4 justify-center flex">
                    <?php
                    use app\model\ProductModel;
                    $products=new ProductModel();
                    $products->RenderProduct();
                    ?>
                </div>
        </div>
    </nav>
    </form>
</header>

