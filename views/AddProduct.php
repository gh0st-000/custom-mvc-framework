<form class="w-full" action="/save" method="post" id="product_form">
    <nav class="border-b border-gray-900">
        <div class="max-w-screen-2xl mx-auto px-2 sm:px-6 lg:px-8">
            <div class="relative flex items-center justify-between h-16">
                <div class="absolute inset-y-0 left-0 flex items-center sm:hidden">
                    <button type="button" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white" aria-controls="mobile-menu" aria-expanded="false">
                        <span class="sr-only">Open main menu</span>
                        <svg class="block h-6 w-6 text-gray-900" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                        </svg>
                    </button>
                </div>
                <div class="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                    <div class="flex-shrink-0 flex items-center">

                    </div>
                    <div class="hidden sm:block">
                        <div class="flex space-x-4">
                            <span  class="text-xl px-3 py-2 font-medium text-sm font-medium" aria-current="page">Add Product</span>
                        </div>
                    </div>
                </div>
                <div class="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                    <div class="ml-3 relative">
                        <div class="flex justify-between space-x-3">
                            <button  type="submit"
                               class="px-3 py-2 bg-gray-800 text-white flex text-sm rounded-md focus:outline-none  border border-white hover:bg-green-600"
                               id="submit" aria-expanded="false" aria-haspopup="true">
                                Save
                            </button>
                            <a href="/"
                                    class="px-3 py-2 bg-gray-800 text-white flex text-sm rounded-md focus:outline-none  border border-white hover:bg-red-600"
                                    id="user-menu-button" aria-expanded="false" aria-haspopup="true">
                                Cancel
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sm:hidden" id="mobile-menu">
            <div class="px-2 pt-2 pb-3 space-y-1 hidden" id="mobile-nav">
                <a href="#" class="bg-gray-900 text-white block px-3 py-2 rounded-md text-base font-medium" aria-current="page">Product List</a>
            </div>
        </div>
    </nav>
       <div class="block w-1/2 mx-auto">
           <span class="justify-center flex py-4 text-xl">Product Information</span>
           <span id="all-field" class="justify-center flex py-4 text-md text-red-400 hidden">All Field Is Required !</span>
           <div class="w-1/2 mx-auto mt-3">
               <div class="grid grid-cols-1 gap-1">
                   <span id="sku-error" class="pb-2 text-red-400 text-sm ml-1 hidden">Sku Is Required</span>
                   <span id="data-sku-error" class="pb-2 text-red-400 text-sm ml-1 hidden">Please, provide the data of indicated type</span>
               </div>
             <input type="text"  placeholder="SKU" class="w-full px-4 py-2 mt-1 rounded-md border" name="sku" id="sku">
           </div>
           <div class="w-1/2 mx-auto mt-3">
               <div class="grid grid-cols-1 gap-1">
               <span id="name-error" class="pb-2 text-red-400 text-sm ml-1 hidden">Name Is Required</span>
               <span id="data-name-error" class="pb-2 text-red-400 text-sm ml-1 hidden">Please, provide the data of indicated type</span>
               </div>
               <input type="text"  placeholder="Name" class="w-full px-4 py-2 rounded-md border" name="name" id="name" >
           </div>
           <div class="w-1/2 mx-auto mt-3">
               <div class="grid grid-cols-1 gap-1">
                   <span id="price-error" class="pb-2 text-red-400 text-sm ml-1 hidden">Price Is Required</span>
                   <span id="data-price-error" class="pb-2 text-red-400 text-sm ml-1 hidden">Please, provide the data of indicated type</span>
               </div>
               <input type="text"  placeholder="Price" class="w-full px-4 py-2 rounded-md border" name="price" id="price" >
           </div>
           <div class="w-1/2 mx-auto mt-3">
               <select id="productType" class="w-full px-4 py-2 rounded-md border">
                   <option>Select Option</option>
                   <option value="0" id="DVD">DVD</option>
                   <option value="1" id="Furniture">Book</option>
                   <option value="2" id="Book">Furniture</option>
               </select>
           </div>
           <div class="w-1/2 mx-auto mt-3 space-y-2" id="hidden_div_dvd" style="display: none;">
               <div class="grid grid-cols-1 gap-1">
                   <span class="pb-2 text-red-400 text-sm ml-1 hidden" id="size-error">MB Is Required</span>
                   <span id="data-size-error" class="pb-2 text-red-400 text-sm ml-1 hidden">Please, provide the data of indicated type</span>
               </div>
               <label class="pb-2 text-sm ml-1">Please Provide Size in MB</label>
               <input type="text" placeholder="Size" class="w-full px-4 py-2 rounded-md border" name="size" id="size" >
           </div>
           <div class="w-1/2 mx-auto mt-3 space-y-2" id="hidden_div_book" style="display: none;" >
               <div class="grid grid-cols-1 gap-1">
                   <span class="pb-2 text-red-400 text-sm ml-1 hidden" id="weight-error">Weight Is Required</span>
                   <span id="data-weight-error" class="pb-2 text-red-400 text-sm ml-1 hidden">Please, provide the data of indicated type</span>
               </div>
               <label class="pb-2 text-sm ml-1">Please Provide  Weight in KG</label>
               <input type="text" placeholder="Book"  class="w-full px-4 py-2 rounded-md border" name="weight" id="weight" >
           </div>
           <div class="w-1/2 mx-auto mt-3 space-y-4" id="hidden_div_furniture" style="display: none;">
               <div class="space-y-2">
                   <div class="grid grid-cols-1 gap-1">
                       <span class="pb-2 text-red-400 text-sm ml-1 hidden" id="height-error">Height Is Required</span>
                       <span id="data-height-error" class="pb-2 text-red-400 text-sm ml-1 hidden">Please, provide the data of indicated type</span>
                   </div>
               <label class="pb-2 text-sm ml-1 ">Please Provide Height</label>
               <input type="text"  placeholder="Height" class="w-full px-4 py-2 rounded-md border" name="height" id="height" >
               </div>
               <div class="space-y-2">
                   <div class="grid grid-cols-1 gap-1">
                       <span class="pb-2 text-red-400 text-sm ml-1 hidden" id="width-error">Width Is Required</span>
                       <span id="data-width-error" class="pb-2 text-red-400 text-sm ml-1 hidden">Please, provide the data of indicated type</span>
                   </div>
               <label class="pb-2 text-sm ml-1">Please Provide Width</label>
               <input type="text"  placeholder="Width" class="w-full px-4 py-2 rounded-md border" name="width" id="width" >
               </div>
               <div class="space-y-2">
                   <div class="grid grid-cols-1 gap-1">
                       <span class="pb-2 text-red-400 text-sm ml-1 hidden" id="length-error">Length Is Required</span>
                       <span id="data-length-error" class="pb-2 text-red-400 text-sm ml-1 hidden">Please, provide the data of indicated type</span>
                   </div>
               <label class="pb-2 text-sm ml-1">Please Provide Length</label>
               <input type="text"  placeholder="Length" class="w-full px-4 py-2 rounded-md border" name="length" id="length" >
               </div>
           </div>
       </div>
</form>

<script src="/js/Validation.js"></script>




