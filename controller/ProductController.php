<?php
namespace app\controller;

use app\Core\Aplication;
use app\Core\ControllerClass;
use app\Core\Request;
use app\model\ProductModel;
use PDO;


class ProductController extends ControllerClass
{

    public function handleProduct(Request $request)
    {
        $model=new ProductModel();
        echo $model->insertData();
    }

    public function AddProduct()
    {
        return $this->render('AddProduct');
    }

    public function ProductList()
    {

        return $this->render('ProductList');
    }

    public function  MassDelete(){
        $deleteProduct=new ProductModel();
        echo $deleteProduct->DeleteProduct();

    }


}